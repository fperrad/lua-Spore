
lua-Spore : a generic ReST client
=================================

Introduction
------------

lua-Spore is a generic ReST client library based on pluggable middlewares,
which supports various [RESTful API Description Language](https://en.wikipedia.org/wiki/Overview_of_RESTful_API_Description_Languages):
- [SPORE](https://github.com/SPORE/specifications) (Specification to a POrtable Rest Environment)
- [Google Discovery Document](http://code.google.com/apis/discovery/)
- [RAML](https://raml.org/)
- [Swagger / OpenAPI](https://www.openapis.org/)
- [WADL](https://www.w3.org/Submission/wadl/)

Links
-----

The homepage is at <https://fperrad.frama.io/lua-Spore>,
and the sources are hosted at <https://framagit.org/fperrad/lua-Spore>.

Copyright and License
---------------------

Copyright (c) 2010-2025 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

